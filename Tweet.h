//
//  Tweet.h
//  codeTask
//
//  Created by Angel Boullon on 02/03/14.
//  Copyright (c) 2014 Angel Boullon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Tweet : NSManagedObject

@property (nonatomic, retain) NSDate * creationDate;
@property (nonatomic, retain) NSDate * expireDate;
@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * screenName;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSNumber * lon;
@property (nonatomic, retain) NSNumber * lat;

@end
