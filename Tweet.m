//
//  Tweet.m
//  codeTask
//
//  Created by Angel Boullon on 02/03/14.
//  Copyright (c) 2014 Angel Boullon. All rights reserved.
//

#import "Tweet.h"


@implementation Tweet

@dynamic creationDate;
@dynamic expireDate;
@dynamic identifier;
@dynamic name;
@dynamic screenName;
@dynamic text;
@dynamic lon;
@dynamic lat;

@end
