//
//  AppDelegate.h
//  codeTask
//
//  Created by Angel Boullon on 01/03/14.
//  Copyright (c) 2014 Angel Boullon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (void)saveContext:(NSManagedObjectContext *)context;
- (NSURL *)applicationDocumentsDirectory;

@end
