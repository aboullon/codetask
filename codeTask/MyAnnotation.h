//
//  MyAnnotation.h
//  codeTask
//
//  Created by Angel Boullon on 03/03/14.
//  Copyright (c) 2014 Angel Boullon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "Tweet.h"

@interface MyAnnotation : NSObject <MKAnnotation>{
    
    CLLocationCoordinate2D coordinate;
    NSString *title;
    NSString *subtitle;
    NSDate *expireDate;
}

@property (nonatomic,readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, retain) NSDate *expireDate;

-(id)initWithTitle:(NSString *)aTitle subtitle:(NSString *)aSubtitle lat:(float)lat lon:(float)lon expireDate:(NSDate *)aExpireDate;

@end
