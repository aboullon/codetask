//
//  Utilities.m
//  codeTask
//
//  Created by Angel Boullon on 03/03/14.
//  Copyright (c) 2014 Angel Boullon. All rights reserved.
//

#import "Utilities.h"
#import "Reachability.h"

@implementation Utilities

+ (BOOL)checkInternet
{
    Reachability* reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    return (remoteHostStatus != NotReachable);
}

@end
