//
//  ViewController.h
//  codeTask
//
//  Created by Angel Boullon on 01/03/14.
//  Copyright (c) 2014 Angel Boullon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface ViewController : UIViewController <MKMapViewDelegate, UISearchBarDelegate>

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;

@property (strong,nonatomic) NSTimer *myTimer;
@property (strong,nonatomic) NSURLConnection *connection;

@end
