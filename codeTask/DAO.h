//
//  DAO.h
//  codeTask
//
//  Created by Angel Boullon on 03/03/14.
//  Copyright (c) 2014 Angel Boullon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Tweet.h"

@interface DAO : NSObject

+(void) insertTweetId:(NSString *) identifier
                 name:(NSString *) name
           screenName:(NSString *) screenName
                 text:(NSString *) text
                  lon:(float) lon
                  lat:(float) lat
         creationDate:(NSDate *) creationDate
           expireDate:(NSDate *) expireDate;

+(void) deleteTweetsExpiredToDate:(NSDate *) date;

+(void) deleteAllTweets;

@end
