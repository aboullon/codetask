//
//  ViewController.m
//  codeTask
//
//  Created by Angel Boullon on 01/03/14.
//  Copyright (c) 2014 Angel Boullon. All rights reserved.
//

#import "ViewController.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import "Tweet.h"
#import "DAO.h"
#import "MyAnnotation.h"
#import "Utilities.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self initMapView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)startStreamingWithKeyword:(NSString *)aKeyword
{
    //First, we need to obtain the account instance for the user's Twitter account
    ACAccountStore *store = [[ACAccountStore alloc] init];
    ACAccountType *twitterAccountType = [store accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    //  Request permission from the user to access the available Twitter accounts
    [store requestAccessToAccountsWithType:twitterAccountType
                                   options:nil
                                completion:^(BOOL granted, NSError *error) {
                                    if (!granted) {
                                        // The user rejected your request
                                        NSLog(@"User rejected access to the account.");
                                    }
                                    else {
                                        // Grab the available accounts
                                        NSArray *twitterAccounts = [store accountsWithAccountType:twitterAccountType];
                                        if ([twitterAccounts count] > 0) {
                                            ACAccount *account = [twitterAccounts lastObject];
                                            
                                            NSURL *url = [NSURL URLWithString:@"https://stream.twitter.com/1.1/statuses/filter.json"];
                                            NSDictionary *params = @{@"track" : aKeyword};
                                            
                                            SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                                                    requestMethod:SLRequestMethodPOST
                                                                                              URL:url
                                                                                       parameters:params];
                                            
                                            [request setAccount:account];
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                self.connection = [NSURLConnection connectionWithRequest:[request preparedURLRequest] delegate:self];
                                                [self.connection start];
                                            });
                                        } // if ([twitterAccounts count] > 0)
                                    } // if (granted)
                                }];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    NSString *response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSError *error;
    NSDictionary *dictTweet = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding]
                                                              options:NSJSONReadingMutableLeaves
                                                                error:&error];
    
    if (!error){
        
        if ((dictTweet[@"coordinates"] != [NSNull null]) && (dictTweet[@"id_str"])){
            // The tweet has coordinates.
            
            // Data for CoreData and Annotation
            NSString *identifier = dictTweet[@"id_str"];
            NSString *name = dictTweet[@"user"][@"name"];
            NSString *screenName = dictTweet[@"user"][@"screen_name"];
            NSString *text = dictTweet[@"text"];
            
            // Info: Inner coordinates field is geoJSon formatted (longitude,latitude).
            float lon = [dictTweet[@"coordinates"][@"coordinates"][0] floatValue];
            float lat = [dictTweet[@"coordinates"][@"coordinates"][1] floatValue];
            
            NSDate *now = [NSDate date];
            
            // Lifespan of the tweet
            float lifespan =  10.0; //10 secs
            NSDate *expireDate = [now dateByAddingTimeInterval:lifespan];
            
            // Add Tweet to CoreData
            [DAO insertTweetId:identifier
                          name:name
                    screenName:screenName
                          text:text
                           lon:lon
                           lat:lat
                  creationDate:now
                    expireDate:expireDate
             ];
            
            // Add Tweet to MapView
            NSString *title = [NSString stringWithFormat:@"%@ (%@)",name, screenName];
            MyAnnotation *ann =  [[MyAnnotation alloc] initWithTitle:title
                                                            subtitle:text
                                                                 lat:lat
                                                                 lon:lon
                                                          expireDate:expireDate];
            [self.mapView addAnnotation:ann];
        }
    }
}

/** Delete old tweets from CoreData and the MapView
 */
-(void) deleteOldTweets {
    
    if ([Utilities checkInternet]) {
        NSDate *now = [NSDate date];
        
        // Delete tweets from DAO
        //[DAO deleteTweetsExpiredToDate:now];
        
        // Delete annotations from the Map
        NSMutableArray *annotations = [self.mapView.annotations mutableCopy];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(expireDate < %@)", now];
        
        [annotations filterUsingPredicate:predicate];
        
        [self.mapView removeAnnotations:annotations];
    }
    
    // Restart the timer
    self.myTimer = [NSTimer scheduledTimerWithTimeInterval:5
                                                    target:self
                                                  selector:@selector(deleteOldTweets)
                                                  userInfo:nil
                                                   repeats:NO];
}


#pragma mark - MapView
/** Initializes the MapView
 */
-(void) initMapView {
    
    self.mapView.delegate = self;
    self.mapView.mapType = MKMapTypeStandard;
    self.mapView.zoomEnabled = YES;
    self.mapView.scrollEnabled = YES;
}

#pragma mark - Search bar
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [searchBar resignFirstResponder];
    
    self.myTimer = [NSTimer scheduledTimerWithTimeInterval:5
                                                    target:self
                                                  selector:@selector(deleteOldTweets)
                                                  userInfo:nil
                                                   repeats:NO];  //5 represents 5 sec.
    
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self startStreamingWithKeyword:self.searchBar.text];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [self.myTimer invalidate];
}

@end
