//
//  MyAnnotation.m
//  codeTask
//
//  Created by Angel Boullon on 03/03/14.
//  Copyright (c) 2014 Angel Boullon. All rights reserved.
//

#import "MyAnnotation.h"

@implementation MyAnnotation 

@synthesize coordinate, title, subtitle, expireDate;


-(id)initWithTitle:(NSString *)aTitle subtitle:(NSString *)aSubtitle lat:(float)lat lon:(float)lon expireDate:(NSDate *)aExpireDate {
    
    self = [super init];
    
    if (self) {
        coordinate = CLLocationCoordinate2DMake(lat, lon);
        title = aTitle;
        subtitle = aSubtitle;
        expireDate = aExpireDate;
    }
    
    return self;
}

@end
