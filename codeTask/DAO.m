//
//  DAO.m
//  codeTask
//
//  Created by Angel Boullon on 03/03/14.
//  Copyright (c) 2014 Angel Boullon. All rights reserved.
//

#import "DAO.h"
#import "AppDelegate.h"

@implementation DAO

/** Insert Tweet in CoreData
 */
+(void) insertTweetId:(NSString *) identifier
                 name:(NSString *) name
           screenName:(NSString *) screenName
                 text:(NSString *) text
                  lon:(float) lon
                  lat:(float) lat
         creationDate:(NSDate *) creationDate
           expireDate:(NSDate *) expireDate {
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];

    Tweet *tweet = (Tweet *)[NSEntityDescription insertNewObjectForEntityForName:@"Tweet" inManagedObjectContext:[appDelegate managedObjectContext]];

    [tweet setIdentifier:identifier];
    [tweet setName:name];
    [tweet setScreenName:screenName];
    [tweet setText:text];
    [tweet setLon:[NSNumber numberWithFloat:lon]];
    [tweet setLat:[NSNumber numberWithFloat:lat]];
    [tweet setCreationDate:creationDate];
    [tweet setExpireDate:expireDate];
    
    [appDelegate saveContext];
}

/** Delete Tweet from CoreData that has expired in reference to the given date
 */
+(void) deleteTweetsExpiredToDate:(NSDate *) date {
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSFetchRequest * fetchReq = [[NSFetchRequest alloc] init];
    [fetchReq setEntity:[NSEntityDescription entityForName:@"Tweet" inManagedObjectContext:context]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(expirationDate < %@)", date];
    [fetchReq setPredicate:predicate];
    
    [fetchReq setIncludesPropertyValues:NO];
    
    NSError * error = nil;
    NSArray * fetchResult = [context executeFetchRequest:fetchReq error:&error];
    
    for (NSManagedObject * tweet in fetchResult) {
        [context deleteObject:tweet];
    }
}

/** Delete all tweets from CoreData
 */
+(void) deleteAllTweets{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSFetchRequest * fetchReq = [[NSFetchRequest alloc] init];
    [fetchReq setEntity:[NSEntityDescription entityForName:@"Tweet" inManagedObjectContext:context]];
    
    [fetchReq setIncludesPropertyValues:NO];
    
    NSError * error = nil;
    NSArray * fetchResult = [context executeFetchRequest:fetchReq error:&error];
    
    for (NSManagedObject * tweet in fetchResult) {
        [context deleteObject:tweet];
    }
}

@end
