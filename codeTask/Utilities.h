//
//  Utilities.h
//  codeTask
//
//  Created by Angel Boullon on 03/03/14.
//  Copyright (c) 2014 Angel Boullon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utilities : NSObject

+ (BOOL)checkInternet;

@end
